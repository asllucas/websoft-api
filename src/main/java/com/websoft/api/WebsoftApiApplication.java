package com.websoft.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebsoftApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebsoftApiApplication.class, args);
	}

}
