package com.websoft.api.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.websoft.api.models.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {
	
	Optional<Produto> findById(Long id);
}
