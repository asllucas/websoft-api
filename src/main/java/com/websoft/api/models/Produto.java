package com.websoft.api.models;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "produtos")
public class Produto {
	
	@Id
	@OrderBy("value ASC")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id_produto;
	
	@Column(length = 100)
	private String descricao;
	
	@Column(length = 2)
	private String unidade;
	
	private Integer quantidade;
	
	@Column(precision = 9, scale = 2)
	private BigDecimal preco_compra;
	
	@Column(precision = 9, scale = 2)
	private BigDecimal preco_venda;
	
	@Column(length = 2)
	private String status;

	public long getId_produto() {
		return id_produto;
	}

	public void setId_produto(long id_produto) {
		this.id_produto = id_produto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getPreco_compra() {
		return preco_compra;
	}

	public void setPreco_compra(BigDecimal preco_compra) {
		this.preco_compra = preco_compra;
	}

	public BigDecimal getPreco_venda() {
		return preco_venda;
	}

	public void setPreco_venda(BigDecimal preco_venda) {
		this.preco_venda = preco_venda;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
